import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgChaynsComponent } from './ng-chayns.component';

describe('NgChaynsComponent', () => {
  let component: NgChaynsComponent;
  let fixture: ComponentFixture<NgChaynsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NgChaynsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NgChaynsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
