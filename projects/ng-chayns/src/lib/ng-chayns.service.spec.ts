import { TestBed } from '@angular/core/testing';

import { NgChaynsService } from './ng-chayns.service';

describe('NgChaynsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: NgChaynsService = TestBed.get(NgChaynsService);
    expect(service).toBeTruthy();
  });
});
