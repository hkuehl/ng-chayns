import { NgModule } from '@angular/core';
import { NgChaynsComponent } from './ng-chayns.component';
import { AccordionComponent } from './accordion/accordion.component';

@NgModule({
  declarations: [NgChaynsComponent, AccordionComponent],
  imports: [
  ],
  exports: [NgChaynsComponent]
})
export class NgChaynsModule { }
